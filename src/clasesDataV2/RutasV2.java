package clasesDataV2;

import clasesData.routes;
import clasesData.trips;

public class RutasV2 {

	private routes ruta;
	private trips viaje;
	
	public RutasV2 (routes pRuta, trips pViaje)
	{
		ruta = pRuta;
		viaje = pViaje;
	}
	
	
	public routes darRuta()
	{
		return ruta;
	}
	
	public trips darViajes()
	{
		return viaje;
	}
	
}
