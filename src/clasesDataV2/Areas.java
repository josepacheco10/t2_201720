package clasesDataV2;

import clasesData.stops;

public class Areas {

	private String area;
	private stops paradas;
	
	
	public Areas (String pArea, stops pParadas)
	{
		area = pArea;
		paradas = pParadas;
	}
	
	
	public String darArea()
	{
		return area;
	}
	
	public stops darParadas()
	{
		return paradas;
	}
	
	
}
