package controller;

import clasesData.routes;
import clasesData.stops;
import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();
	
	public static void loadRoutes(String routesFile) {
		manager.loadRoutes(routesFile);
	}
		
	public static void loadTrips(String tripsFile) {
		manager.loadTrips(tripsFile);
	}

	public static void loadStopTimes(String stopTimesFile) {
		manager.loadStopTimes(stopTimesFile);
	}
	
	public static void loadStops(String stopsFile) {
		manager.loadStops(stopsFile);
	}
	
	public static IList<routes> routeAtStop(String stopName) {
		return null;
	}
	
	public static IList<stops> stopsRoute(String routeName, String direction) {
		return null;
	}
}
