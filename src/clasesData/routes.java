package clasesData;

public class routes {

	private int routeId;
	private String agencyId;
	private String routeShortName;
	private String routeLongName;
	private String routeDesc;
	private int routeType;
	private String routeUrl;
	private String routeColor;
	private String routeTextColor;

	public routes(int pRouteId, String pAgencyId, String pRouteShortName,
			String pRouteLongName, String pRouteDesc, int pRouteType,
			String pRouteUrl, String pRouteColor) {
		
		routeId = pRouteId;
		agencyId = pAgencyId;
		routeShortName = pRouteShortName;
		routeLongName = pRouteLongName;
		routeDesc = pRouteDesc;
		routeType = pRouteType;
		routeUrl = pRouteUrl;
		routeColor = pRouteColor;

	}

	public int getRouteId() {
		return routeId;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public String getRouteShortName() {
		return routeShortName;
	}

	public String getRouteLongName() {
		return routeLongName;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public int getRouteType() {
		return routeType;
	}

	public String getRouteUrl() {
		return routeUrl;
	}

	public String getRouteColor() {
		return routeColor;
	}

	public String getRouteTextColor() {
		return routeTextColor;
	}
}
