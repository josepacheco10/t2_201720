package clasesData;

import model.data_structures.Node;

public class stops {
	
	private int stopId;
	private int stopCode;
	private String stopName;
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String zoneId;
	private String stopUrl;
	private int locationType;
	
	private routes r; 
	//private String parentStation;
	
	public stops(int pStopId, int pStopCode, String pStopName, String pStopDesc, double pStopLat, double pStopLon, String pZoneId, String pStopUrl, int pLocationType)
	{
		stopId = pStopId;
		stopCode = pStopCode;
		stopName = pStopName;
		stopDesc = pStopDesc;
		stopLat = pStopLat;
		stopLon = pStopLon;
		zoneId = pZoneId;
		stopUrl = pStopUrl;
		locationType = pLocationType;
		//pParentStation = pParentStation;
		
		r = null;
	}
	
	public int getStopId()
	{
		return stopId;
	}
	
	public int getStopCode()
	{
		return stopCode;
	}
	
	public String getStopName()
	{
		return stopName;
	}
	
	public String getStopDesc()
	{
		return stopDesc;
	}

	public double getStopLat()
	{
		return stopLat;
	}
	
	public double getStopLon()
	{
		return stopLon;
	}
	
	public String getZoneId()
	{
		return zoneId;
	}
	
	public String getStopUrl()
	{
		return stopUrl;
	}
	
	public int getLocationType()
	{
		return locationType;
	}
	
	public String getParentStation()
	{
		return "m";
	}
	
	public routes getRoute()
	{
		return r;
	}
}
