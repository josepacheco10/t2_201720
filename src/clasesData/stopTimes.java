package clasesData;

import java.util.Calendar;

public class stopTimes {
	
	private int tripId;
	private String arrivalTime;
	private String departureTime;
	private int stopId;
	private int stopSequence;
	private int stopHeadSign;
	private int pickupType;
	private int dropOffType;
	private double shapeDistTraveled;
	
	public stopTimes(int pTripId, String pArrivalTime, String pDepartureTime, int pStopId, int pStopSequence, int pStopHeadSign, int pPickupType, int pDropOffType, double pShapeDistTraveled)
	{
		tripId = pTripId;
		arrivalTime = pArrivalTime;
		departureTime = pDepartureTime;
		stopId = pStopId;
		stopSequence = pStopSequence;
		stopHeadSign = pStopHeadSign;
		pickupType = pPickupType;
		dropOffType = pDropOffType;
		shapeDistTraveled = pShapeDistTraveled;
	}
	
	public int getTripId()
	{
		return tripId;
	}
	
	public String getArrivalTime()
	{
		return arrivalTime;
	}
	
	public String getDepartureTime()
	{
		return departureTime;
	}
	
	public int getStopId()
	{
		return stopId;
	}
	
	public int getStopSequence()
	{
		return stopSequence;
	}
	
	public int getStopHeadSign()
	{
		return stopHeadSign;
	}
	
	public int getPickUpType()
	{
		return pickupType;
	}
	
	public int getDropOffType()
	{
		return dropOffType;
	}
	
	public double getShapeDistTraveled()
	{
		return shapeDistTraveled;
	}

}
