package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;



import java.util.Iterator;

import clasesData.routes;
import clasesData.stopTimes;
import clasesData.stops;
import clasesData.trips;
import clasesDataV2.Areas;
import clasesDataV2.RutasV2;
import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	private IList<routes> rutas;
	private IList<trips> viajes;
	private IList<stopTimes> stopsT;
	private IList<stops> m;
	private IList<Areas> areas;
	
	private IList<RutasV2> rutasV2;
	
	private routes upRutas;
	
	
	public STSManager()
	{
		m = new RingList<stops>();
		areas = new RingList<Areas>();
		rutas = new DoubleLinkedList<routes>();
		viajes = new RingList<trips>();
	
		rutasV2 = new RingList<RutasV2>();
	}
	
	public static final String SEPARATOR = ",";

	public void loadRoutes(String routesFile) {
		BufferedReader br = null;

		try {

			try 
			{
				br = new BufferedReader(new FileReader(routesFile));
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			String line = br.readLine();
			line = br.readLine();

			while (null != line) {
				
				String[] fields = line.split(SEPARATOR);
				line = br.readLine();

				int route_id = Integer.parseInt(fields[0]);
				String agency = fields[1];
				String sN = fields[2];
				String lN = fields[3];
				String rD = fields[4];
				int rT = Integer.parseInt(fields[5]);
				String url = fields[6];
				//String dropOffType = fields[7];
				String pR = fields[8];
				
				routes nueva = new routes(route_id, agency, sN, lN, rD, rT, 
						url, pR);
				
				upRutas = nueva;
				
				rutas.add(nueva);				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}


	public void loadTrips(String tripsFile) {
		
		BufferedReader br = null;

		try {

			try 
			{
				br = new BufferedReader(new FileReader(tripsFile));
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			String line = br.readLine();
			line = br.readLine();

			while (null != line) {
				
				String[] fields = line.split(SEPARATOR);
				line = br.readLine();

				int route_id = Integer.parseInt(fields[0]);
				int servicios = Integer.parseInt(fields[1]);
				int tripId = Integer.parseInt(fields[2]);
				String tHead = fields[3];
				String tSN = fields[4];
				int directionId = Integer.parseInt(fields[5]);
				int blockId = Integer.parseInt(fields[6]);
				int shapedId = Integer.parseInt(fields[7]);
				int wcA = Integer.parseInt(fields[8]);
				int bA = Integer.parseInt(fields[9]);
				
				trips nueva = new trips(route_id, servicios, tripId, tHead, tSN, directionId , blockId,
						shapedId, wcA, bA);
				
				viajes.add(nueva);
				
				
				RutasV2 novo = new RutasV2(upRutas, nueva);
				
				rutasV2.add(novo);
				
				//System.out.println(viajes.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	/**
	 * Carga los horarios de las paradas a partir de un archivo de texto.
	 */
	public void loadStopTimes(String stopTimesFile) {

		BufferedReader br = null;

		try {

			try 
			{
				br = new BufferedReader(new FileReader(stopTimesFile));
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			String line = br.readLine();

			while (null != line) {
				
				String[] fields = line.split(SEPARATOR);
				line = br.readLine();

				int tripId = Integer.parseInt(fields[0]);
				String arrivalTime = fields[1];
				String departureTime = fields[2];
				int stopId = Integer.parseInt(fields[3]);
				int stopSequence = Integer.parseInt(fields[4]);
				int stopHeadSign = Integer.parseInt(fields[5]);
				int pickupType = Integer.parseInt(fields[6]);
				int dropOffType = Integer.parseInt(fields[7]);
				double shapeDistTraveled = Double.valueOf(fields[8]);
				
				stopTimes nueva = new stopTimes(tripId, arrivalTime, departureTime, stopId, stopSequence, stopHeadSign,
						pickupType, dropOffType, shapeDistTraveled);
				
				stopsT.add(nueva);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Carga las paradas a partir de un archivo de texto.
	 * Adem�s, asocia a una lista de tipo anillo de zonas_id todas las paradas correspondientes a la zona.
	 */
	public void loadStops(String stopsFile) {

		BufferedReader br = null;

		try {

			br = new BufferedReader(new FileReader(stopsFile));
			String line = br.readLine();
			line = br.readLine();
			while (line != null) 
			{
				String[] fields = line.split(SEPARATOR);
				
				int stopId = Integer.parseInt(fields[0]);
				int stopCode = fields[1].equals(" ") ? 000 : Integer.parseInt(fields[1]);
				String stopName = fields[2];
				String stopDesc = fields[3];
				double stopLat = Double.valueOf(fields[4]);
				double stopLon = Double.valueOf(fields[5]);
				String zoneId = fields[6];
				String stopUrl = fields[7];
				int locationType = Integer.parseInt(fields[8]);

				stops nueva = new stops(stopId, stopCode, stopName, stopDesc, 
						stopLat, stopLon, zoneId, stopUrl, locationType);
				
				Areas novo = new Areas(zoneId, nueva);
				areas.add(novo);
				
				//System.out.println(areas.getSize());
				
				m.add(nueva);
				line = br.readLine();

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}


		
	public IList<routes> routeAtStop(String stopName) {

		DoubleLinkedList<routes> rutas = new DoubleLinkedList<routes>();
		Node<stops> aux = (Node<stops>) m;
		
		while (aux != null)
		{
			if (aux.darItem().getStopName() == stopName)
			{
				rutas.add(aux.darItem().getRoute());
			}
			aux = aux.getNext();
		}
		return rutas;
	}

	public IList<stops> stopsRoute(String routeName, String direction) {

		DoubleLinkedList<stops> rutas = new DoubleLinkedList<stops>();
		String nombreR = null;
		
//		Node<trips> aux = (Node<trips>) viajes;
//		Node<routes> auxV0 = (Node<routes>) rutas;
//		
//		while (aux!= null && auxV0 != null)
//		{
//			if (aux.darItem().getRouteId() == auxV0.darItem().getRouteId() )
//			{
//				nombreR = auxV0.darItem().getRouteShortName();
//			}
//			
//		}
		
		return rutas;
	}

}
