package model.data_structures;

public class Node <T>{

	public Node<T> next;
	public Node<T> previous;
	public T item;

	public Node(Node<T> n,Node<T> k, T pItem)
	{
		next = n;
		previous = k;
		item=pItem;
	}
	

	public Node<T> getNext()
	{
		return next;
	}
	public Node<T> getPrevious()
	{
		return previous;
	}
	
	public T darItem ()
	{
		return item;
	}
	
	////////////////////////////////////////////////////
	//				  MODIFICADORES					////
	////////////////////////////////////////////////////
	
	public void setNext(Node<T> newN)
	{
		next = newN;
	}
	public void setPrevious(Node<T> newN)
	{
		previous= newN;
	}


}