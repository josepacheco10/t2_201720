package model.data_structures;

import java.util.Iterator;

import model.data_structures.Node;

public class RingList<T> implements IList<T> {

	// //////////////////////////////////////////////////
	// Atributos ////
	// //////////////////////////////////////////////////

	/**
	 * Cabeza de la lista, primer elemento.
	 */
	private Node<T> first;

	/**
	 * Ultimo elemento de la lista.
	 */
	private Node<T> last;

	/**
	 * Elemento actual en la lista.
	 */
	private Node<T> actual;

	/**
	 * Elemento sucesor en la lista.
	 */
	private Node<T> sig;

	/**
	 * Elemento sucesor en la lista.
	 */
	private Node<T> prev;

	/**
	 * Tamanio de la lista, cantidad de nodos.
	 */
	private Integer size;

	// //////////////////////////////////////////////////
	// Constructor ////
	// //////////////////////////////////////////////////

	public RingList() {
		first = null;
		last = null;
		actual = null;
		sig = null;
		prev = null;
		size = 0;
	}

	// //////////////////////////////////////////////////
	// M�todos ////
	// //////////////////////////////////////////////////

	/**
	 * Retorna el tamanio de la lista.
	 * 
	 * @return Tamanio de la lista.
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * Dice si la lista esta vacia
	 * 
	 * @return True si la lista esta vacia.
	 */
	public boolean isEmpty() {
		boolean iE = false;

		if (first == null) {
			iE = true;
		}
		return iE;
	}

	/**
	 * Retorna la cabeza de la lista, el primer elemento.
	 * 
	 * @return Primer elemento en toda la lista.
	 */
	public Node<T> getFirst() {
		return isEmpty() ? null : first;
	}

	/**
	 * Retorna el �ltimo elemento de la lista.
	 * 
	 * @return Ultimo elemento en toda la lista.
	 */
	public Node<T> getLast() {
		return isEmpty() ? null : last;
	}

	/**
	 * Agrega un elemento al inicio de la lista. 
	 * @param nElement. Nodo a agregar
	 */
	public void add(T nElement) {

		Node<T> nuevo = new Node<T>(null, null, nElement);

		if (isEmpty()) {
			first = nuevo;
			first.setPrevious(null);
			first.setNext(null);

			size++;
		} else {
			
			
			nuevo.setNext(first);
			//last.setNext(first);
			nuevo.setPrevious(last);
			
			first = nuevo;

			size++;
		}

	}

	public void addAtk(T nElement, int pos) throws Exception {
		
		Node<T> nuevo= new Node<T>(null, null, nElement);
		
		Integer num = 0;
        actual = first;
        
        if(first == null)
        {
            add(nElement);   
        }
        
        else
        {
        	
        	if (pos == 0 || pos%size == 0)
        	{
        		add(nElement); 
        	}
        	
        	else if (pos > size)
        	{
        	
        		int posX = pos%size;
        		int contador=1;
        		Node<T> aux = first;
        		
        		
        		while(aux!=null)
        		{
        			if (contador==posX )
        			{
        				Node<T> auxV2 = aux.getNext();
		        		
		        		nuevo.next = auxV2;
		        		nuevo.previous = aux;
		        		
		        		aux.next = nuevo;
		        		auxV2.previous = nuevo;
		        		
		        		break;
        			}
        			
        			aux = aux.next;
        			contador++;
        		}
        			size++;
        		}
        		else
        		{
        			int cont = 1;
	        		Node<T> aux = first;
	        		
	        		while(aux!=null)
		        	{
		        		
	        			if (cont == pos)
		        		{
			        		
			        		Node<T> auxV2 = aux.next;
			        		
			        		nuevo.next = auxV2;
			        		nuevo.previous = aux;
			        		
			        		aux.next = nuevo;
			        		auxV2.previous = nuevo;
			        		
			        		break;
		        		}
		        		
	        			aux = aux.next;
		        		cont++;
	        		}
	        		
	        		size++;
			}
        }

		
	}

	public void addLast(T nElement) 
	{
		Node<T> nuevo= new Node<T>(null, null, nElement);

		if(first==null)
		{
			first = nuevo;
			last = nuevo;
			
			size++;

		}
		else 
		{
			nuevo.next = first;
			nuevo.previous = last;
			last.next = nuevo;
			first.previous= nuevo;
			last = nuevo;
			
			size++;
		}

	}

	public void delete(Node<T> nElement) throws Exception {
		// TODO Auto-generated method stub

	}

	public void deleteAtk(Node<T> nElement, int pos) throws Exception {
		// TODO Auto-generated method stub

	}

	public T getElement(Node<T> nElement)  {
		
		Node<T> ret = null;
		
		if (nElement == first) 
		{
			ret = first;
		} 
		
		else if (nElement == last) 
		{
			int contador = 0;
			Node<T> aux = first;
			
			while (aux != null) {
				if (aux == nElement) {
					ret = aux;
					break;
				}
				
				aux = aux.next;
				contador++;
			}
		} else {
			int contador = 0;
			Node<T> aux = first;
			
			while (aux != null) {
				if (aux == nElement) {
					ret = aux;
					break;
				}
				aux = aux.next;
				contador++;

			}

		}
		return ret.item;
	}
    


	public T getCurrentElement() {
		return actual.darItem();
	}

	public Node<T> next() {
		return actual.getNext();
	}

	public Node<T> previous() {
		return actual.getPrevious();
	}

	
	public Iterator<T> iterator() 
	{
	return new RingListIterator();
	}
	
	private class RingListIterator implements Iterator<T>
	{
	
		Node<T> actual = first;

	public boolean hasNext()
	{
		return actual!=null;
	}
	public T next()
	{
	T item = actual.item;
	actual = actual.next;
	return item;
	}
	@Override
	public void remove() 
	{
	throw new UnsupportedOperationException(); 

	}


	}
}
