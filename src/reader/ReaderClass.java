package reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ReaderClass {

	
	public static final String SEPARATOR=",";
	public static final String QUOTE="\"";
	
	public static void main(String[] args) throws IOException {

	      BufferedReader br = null;
	      
	      try {
	         
	         br =new BufferedReader(new FileReader("data/stops.txt"));
	         String line = br.readLine();
	         while (null!=line) {
	            String [] fields = line.split(SEPARATOR);
	            System.out.println(Arrays.toString(fields));
	            
	            
	            line = br.readLine();
	         }
	         
	      } catch (Exception e) 
	      {
	         
	      } finally {
	         if (null!=br) {
	            br.close();
	         }
	      }
	
}
	
}
