package test;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;
import junit.framework.TestCase;



public class DoubleLinkedListTest extends TestCase {

	private DoubleLinkedList<Integer> list;

	public void setupEscenario1() {
		list = new DoubleLinkedList();

	}

	public void setupEscenario2() {
		setupEscenario1();

		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		list.add(8);
		list.add(9);
		list.add(10);
	}


	/**
	 * Este metodo permite probar tanto el metodo getSize() como el metodo add()
	 */
	public void testGetSize() {
		setupEscenario2();
		int cantidad = list.getSize();
		assertEquals("No se a�adieron todos los elementos", 10, cantidad);
	}

	public void testGetElement() {

		setupEscenario2();

		Node<Integer> m = new  Node<Integer>(null, null, 7);
		int numero = list.getElement(m);

		assertEquals("No se consiguio el elemento correcto", 3, numero);

	}

	public void testDelete() throws Exception {
		//		setupEscenario2();
		//		list.delete(4);
		//		int cant = list.getSize();
		//		assertEquals("No se elimino nada", 9, cant);
	}

	public void testDeleteAtK() throws Exception {
		//		setupEscenario2();
		//		Node prev = list.getLast();
		//		Node nuevo = new Node(null, prev, null);
		//		list.addLast(nuevo);
		//		list.deleteAtk(nuevo, 11);
		//		int cant = list.getSize();
		//		assertEquals("No se elimino nada", 10, cant);
	}
	//
	//	public void testAddAtK() {
	//		setupEscenario2();
	//		anillo.addAtK(30, 29);
	//		int elemento = list.getElement(0);
	//		assertEquals("No corresponde a lo pedido", 29, elemento);
	//	}

}