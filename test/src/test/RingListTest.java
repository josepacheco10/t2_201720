package test;

import model.data_structures.Node;
import model.data_structures.RingList;
import junit.framework.TestCase;

public class RingListTest extends TestCase {

	private RingList<Integer> anillo;

	public void setupEscenario1() {
		anillo = new RingList();

	}

	public void setupEscenario2() {
		setupEscenario1();

		anillo.add(1);
		anillo.add(2);
		anillo.add(3);
		anillo.add(4);
		anillo.add(5);
		anillo.add(6);
		anillo.add(7);
		anillo.add(8);
		anillo.add(9);
		anillo.add(10);
	}

	public void testRingList() {
		setupEscenario1();
		assertTrue("No se inicializo correctamente el iterador", anillo.iterator() != null);
	}

	/**
	 * Este metodo permite probar tanto el metodo getSize() como el metodo add()
	 */
	public void testGetSize() {
		setupEscenario2();
		int cantidad = anillo.getSize();
		assertEquals("No se a�adieron todos los elementos", 10, cantidad);
	}

	public void testGetElement() {
		
		setupEscenario2();
		
		Node<Integer> m = new  Node<Integer>(null, null, 7);
		int numero = anillo.getElement(m);

		assertEquals("No se consiguio el elemento correcto", 3, numero);

	}

//	public void testDelete() {
//		setupEscenario2();
//		anillo.delete(4);
//		int cant = anillo.getSize();
//		assertEquals("No se elimino nada", 9, cant);
//	}
//
//	public void testDeleteAtK() {
//		setupEscenario2();
//		anillo.deleteAtK(4);
//		int cant = anillo.getSize();
//		assertEquals("No se elimino nada", 9, cant);
//	}
//
//	public void testAddAtK() {
//		setupEscenario2();
//		anillo.addAtK(30, 29);
//		int elemento = anillo.getElement(0);
//		assertEquals("No corresponde a lo pedido", 29, elemento);
//	}

}